package com.yatra.qa.utils;

import static com.jayway.restassured.RestAssured.given;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.restassured.builder.RequestSpecBuilder;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;

public class JSONUtil {
	String apiUrl, apiBody;

	static Logger logger = LoggerFactory.getLogger(JSONUtil.class);

	public static String jsonPost(String APIUrl, String APIBody) {


		logger.info("Iniside jsonPost");

		// Creating object for JSON request
		RequestSpecBuilder builder = new RequestSpecBuilder();

		// Set Request Body
		builder.setBody(APIBody);

		// Set Type of body sending to the request
		builder.setContentType("application/json; charset=UTF-8");

		// Building request
		RequestSpecification requestSpec = builder.build();

		// Hitting request using post method
		Response response = given().authentication().preemptive().basic("", "")
				.spec(requestSpec).when().post(APIUrl);

		// Saving response and return
		String res = response.body().asString();
		return res;
	}

	public static String jsonPostWithUserNamePassword(String APIUrl,
			String APIBody, String userName, String password) {

		// Creating object for JSON request
		RequestSpecBuilder builder = new RequestSpecBuilder();

		// Set Request Body
		builder.setBody(APIBody);

		// Set Type of body sending to the request
		builder.setContentType("application/json; charset=UTF-8");

		// Building request
		RequestSpecification requestSpec = builder.build();

		// Hitting request using post method
		Response response = given().authentication().preemptive()
				.basic(userName, password).spec(requestSpec).when()
				.post(APIUrl);

		// Saving response and return
		String res = response.body().asString();
		return res;
	}

	public static String jsonPostWithoutAuth(String APIUrl, String APIBody) {

		// Creating object for JSON request
		RequestSpecBuilder builder = new RequestSpecBuilder();

		// Set Request Body
		builder.setBody(APIBody);

		// Set Type of body sending to the request
		builder.setContentType("application/json; charset=UTF-8");

		// Building request
		RequestSpecification requestSpec = builder.build();

		Response response = given().spec(requestSpec).post(APIUrl);

		// Saving response and return
		String res = response.body().asString();
		return res;
	}

	public static String jsonGet(String Url) {
		// Creating object for JSON request
		//RequestSpecBuilder builder = new RequestSpecBuilder();

		// Building request
		//RequestSpecification requestSpec = builder.build();

		// Hitting request using get method
		Response response = given().get(Url);
		String res = response.body().asString();
		return res;
	}

}
