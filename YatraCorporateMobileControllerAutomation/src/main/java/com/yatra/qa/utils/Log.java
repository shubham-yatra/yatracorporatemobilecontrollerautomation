package com.yatra.qa.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.SkipException;

/**
 * Log class consists methods for writing log
 * status as pass/fail logs,methods to print test case info and messages
 *
 */
public class Log {

	public static boolean printconsoleoutput;

	static final String TEST_TITLE_HTML_BEGIN = "&emsp;<div class=\"test-title\"> <strong><font size = \"3\" color = \"#000080\">";
	static final String TEST_TITLE_HTML_END = "</font> </strong> </div>&emsp;<div><strong>Steps:</strong></div>";

	static final String TEST_COND_HTML_BEGIN = "&emsp;<div class=\"test-title\"> <strong><font size = \"3\" color = \"#0000FF\">";
	static final String TEST_COND_HTML_END = "</font> </strong> </div>&emsp;";

	static final String MESSAGE_HTML_BEGIN = "<div class=\"test-message\">&emsp;";
	static final String MESSAGE_HTML_END = "</div>";

	static final String PASS_HTML_BEGIN = "<div class=\"test-result\"><br><font color=\"green\"><strong> ";
	static final String PASS_HTML_END1 = " </strong></font> ";
	static final String PASS_HTML_END2 = "</div>&emsp;";

	static final String FAIL_HTML_BEGIN = "<div class=\"test-result\"><br><font color=\"red\"><strong> ";
	static final String FAIL_HTML_END1 = " </strong></font> ";
	static final String FAIL_HTML_END2 = "</div>&emsp;";

	static final String SKIP_EXCEPTION_HTML_BEGIN = "<div class=\"test-result\"><br><font color=\"orange\"><strong> ";
	static final String SKIP_HTML_END1 = " </strong></font> ";
	static final String SKIP_HTML_END2 = " </strong></font> ";

	static final String EVENT_HTML_BEGIN = "<div class=\"test-event\"> <font color=\"maroon\"> <small> &emsp; &emsp;--- ";
	static final String EVENT_HTML_END = "</small> </font> </div>";

	static final String TRACE_HTML_BEGIN = "<div class=\"test-event\"> <font color=\"brown\"> <small> &emsp; &emsp;--- ";
	static final String TRACE_HTML_END = "</small> </font> </div>";

	/**
	 * every suite execution and also sets up the print console flag for the run
	 */
	static {

		try {
			//Properties props = new Properties();
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
			System.setProperty("current.date.time", dateFormat.format(new Date()));
		} catch (Exception e) {
			e.printStackTrace();
		}

		BasicConfigurator.configure();
		//DOMConfigurator.configure("./src/main/resources/log4j.xml");
		//PropertyConfigurator.configure("./src/main/resources/log4j.properties");


		final Map<String, String> params = Reporter.getCurrentTestResult().getTestContext().getCurrentXmlTest()
				.getAllParameters();

		if (params.containsKey("printconsoleoutput")) {
			Log.printconsoleoutput = Boolean.parseBoolean(params.get("printconsoleoutput"));
		}

	} // static block



	/**
	 * lsLog4j returns name of the logger from the current thread
	 */
	public static Logger lsLog4j() {
		return Logger.getLogger(Thread.currentThread().getName());
	}

	/**
	 * callerClass method used to retrieve the Class Name
	 */
	public static String callerClass() {
		return Thread.currentThread().getStackTrace()[2].getClassName();
	}

	/**
	 * testCaseInfo method print the description of the test case in the log
	 * (level=info)
	 * 
	 * @param description
	 *            test case
	 */
	public static void testCaseInfo(String description) {

		lsLog4j().setLevel(Level.ALL);
		lsLog4j().info("");
		lsLog4j().log(callerClass(), Level.INFO, "****             " + description + "             *****", null);
		lsLog4j().info("");

		if (Reporter.getOutput(Reporter.getCurrentTestResult()).toString().contains("<div class=\"test-title\">")) {
			Reporter.log(TEST_TITLE_HTML_BEGIN + description + TEST_TITLE_HTML_END + "&emsp;");
		} else {
			Reporter.log(TEST_TITLE_HTML_BEGIN + description + TEST_TITLE_HTML_END + "<!-- Report -->&emsp;");
		}
		ExtentReporterUtil.testCaseInfo(description);
	}


	public static void testCaseInfo(HashMap<String, String> testdata) {
		String description = testdata.containsKey("Description") ? testdata.get("Description").trim() : null;
		String bugId = testdata.containsKey("BugId") ? testdata.get("BugId") : null;
		String comments = testdata.containsKey("Comments") ? testdata.get("Comments") : null;

		lsLog4j().setLevel(Level.ALL);
		lsLog4j().info("");
		lsLog4j().log(callerClass(), Level.INFO, "****             " + description + "             *****", null);
		lsLog4j().info("");

		if (Reporter.getOutput(Reporter.getCurrentTestResult()).toString().contains("<div class=\"test-title\">")) {
			if (bugId != null && comments != null)
				Reporter.log(TEST_TITLE_HTML_BEGIN + description + " BUGID:" + bugId + " COMMENTS:" + comments
						+ TEST_TITLE_HTML_END + "&emsp;");
			else if (bugId != null && comments == null)
				Reporter.log(TEST_TITLE_HTML_BEGIN + description + " BUGID:" + bugId + TEST_TITLE_HTML_END + "&emsp;");
			else if (bugId == null && comments != null)
				Reporter.log(
						TEST_TITLE_HTML_BEGIN + description + " COMMENTS:" + comments + TEST_TITLE_HTML_END + "&emsp;");
			else
				Reporter.log(TEST_TITLE_HTML_BEGIN + description + TEST_TITLE_HTML_END + "&emsp;");
		} else {
			if (bugId != null && comments != null)
				Reporter.log(TEST_TITLE_HTML_BEGIN + description + " BUGID:" + bugId + " COMMENTS:" + comments
						+ TEST_TITLE_HTML_END + "<!-- Report -->&emsp;");
			else if (bugId != null && comments == null)
				Reporter.log(TEST_TITLE_HTML_BEGIN + description + " BUGID:" + bugId + TEST_TITLE_HTML_END
						+ "<!-- Report -->&emsp;");
			else if (bugId == null && comments != null)
				Reporter.log(TEST_TITLE_HTML_BEGIN + description + " COMMENTS:" + comments + TEST_TITLE_HTML_END
						+ "<!-- Report -->&emsp;");
			else
				Reporter.log(TEST_TITLE_HTML_BEGIN + description + TEST_TITLE_HTML_END + "<!-- Report -->&emsp;");
		}
		ExtentReporterUtil.testCaseInfo(description);
		Log.message(testdata.toString());
	}

	/**
	 * testConditionInfo method print the info of the test case in the log
	 * 
	 * @param description
	 *            test case
	 */
	public static void testConditionInfo(String description) {
		Reporter.log(TEST_COND_HTML_BEGIN + description + TEST_COND_HTML_END);
	}

	/**
	 * Outputs Pass/Fail message in the test log as per the test step outcome
	 */
	public static void testCaseResult() {

		if (Reporter.getOutput(Reporter.getCurrentTestResult()).toString().contains("FAILSOFT")) {
			fail("Test Failed. Check the steps above in red color.");
		} else if (Reporter.getOutput(Reporter.getCurrentTestResult()).toString().contains("FAIL")) {
			fail("Test Failed. Check the steps above in red color.");
		} else {
			pass("Test Passed.");
		}

	}

	/**
	 * endTestCase to print log in the console as a part of ending the test case
	 */
	public static void endTestCase() {
		lsLog4j().info("****             " + "-E---N---D-" + "             *****");
		ExtentReporterUtil.endTest();
	}

	/**
	 * message print the test case custom message in the log (level=info)
	 * 
	 * @param description
	 *            test case
	 */
	public static void message(String description) {
		if(description.contains(">")){

		}
		else{
			Reporter.log(MESSAGE_HTML_BEGIN + description + MESSAGE_HTML_END);
			lsLog4j().log(callerClass(), Level.INFO, description, null);
			ExtentReporterUtil.info(description);
		}


	}
	public static void messageToLogFile(String description) {

		Reporter.log(MESSAGE_HTML_BEGIN + description + MESSAGE_HTML_END);
		lsLog4j().log(callerClass(), Level.INFO, description, null);

	}
	public static void messageToExtentReports(String description) {

		Reporter.log(MESSAGE_HTML_BEGIN + description + MESSAGE_HTML_END);
		ExtentReporterUtil.info(description);

	}

	/**
	 * event print the page object custom message in the log which can be seen
	 * through short cut keys used during debugging (level=info)
	 * 
	 * @param description
	 *            test case
	 */
	public static void event(String description) {

		String currDate = new SimpleDateFormat("dd MMM HH:mm:ss SSS").format(Calendar.getInstance().getTime());
		Reporter.log(EVENT_HTML_BEGIN + currDate + " - " + description + EVENT_HTML_END);
		lsLog4j().log(callerClass(), Level.DEBUG, description, null);
		ExtentReporterUtil.debug(description);
	}
	public static void eventToLogFile(String description) {

		String currDate = new SimpleDateFormat("dd MMM HH:mm:ss SSS").format(Calendar.getInstance().getTime());
		Reporter.log(EVENT_HTML_BEGIN + currDate + " - " + description + EVENT_HTML_END);
		lsLog4j().log(callerClass(), Level.DEBUG, description, null);
	}

	public static void eventToExtentReports(String description) {

		String currDate = new SimpleDateFormat("dd MMM HH:mm:ss SSS").format(Calendar.getInstance().getTime());
		Reporter.log(EVENT_HTML_BEGIN + currDate + " - " + description + EVENT_HTML_END);
		ExtentReporterUtil.debug(description);
	}

	/**
	 * event print the page object custom message in the log which can be seen
	 * through short cut keys used during debugging (level=info)
	 * 
	 * @param description
	 *            test case
	 */
	public static void trace(String description) {

		String currDate = new SimpleDateFormat("dd MMM HH:mm:ss SSS").format(Calendar.getInstance().getTime());
		Reporter.log(TRACE_HTML_BEGIN + currDate + " - " + description + TRACE_HTML_END);
		lsLog4j().log(callerClass(), Level.TRACE, description, null);

	}

	/**
	 * pass print test case status as Pass with custom message (level=info)
	 * 
	 * @param description
	 *            custom message in the test case
	 */
	public static void pass(String description) {

		Reporter.log(PASS_HTML_BEGIN + description + PASS_HTML_END1 + PASS_HTML_END2);
		lsLog4j().log(callerClass(), Level.INFO, description, null);
		ExtentReporterUtil.pass(description);
	}


	/**
	 * fail print test case status as Fail with custom message (level=error)
	 * 
	 * @param description
	 *            custom message in the test case
	 */
	public static void fail(String description) {
		lsLog4j().log(callerClass(), Level.ERROR, description, null);
		Reporter.log("<!--FAIL-->");
		Reporter.log(FAIL_HTML_BEGIN + description + FAIL_HTML_END1 + FAIL_HTML_END2);
		ExtentReporterUtil.fail(description);
		ExtentReporterUtil.logStackTrace(new AssertionError(description));
		Assert.fail(description);
	}

	/**
	 * hasFailSofts returns true if the test steps contains any fail
	 * 
	 * @return boolean
	 */
	public static boolean hasFailSofts() {
		return Reporter.getOutput(Reporter.getCurrentTestResult()).toString().contains("FAILSOFT");
	}

	/**
	 * failsoft print test case step failure as fail and let execution continue
	 * (level=error)
	 * 
	 * @param description
	 *            custom message in the test case
	 */
	public static void failsoft(String description) {
		Reporter.log("<!--FAILSOFT-->");
		Reporter.log("<div class=\"test-result\"><font color=\"red\">&emsp;" + description + "</font></div>");
		ExtentReporterUtil.fail(description);
		lsLog4j().log(callerClass(), Level.ERROR, description, null);
	}

	/**
	 * exception prints the exception message as fail/skip in the log
	 * (level=fatal)
	 * 
	 * @param e
	 *            exception message
	 * @throws Exception
	 */
	public static void exception(Exception e) throws Exception {

		String eMessage = e.getMessage();

		if (eMessage != null && eMessage.contains("\n")) {
			eMessage = eMessage.substring(0, eMessage.indexOf("\n"));
		}
		lsLog4j().log(callerClass(), Level.FATAL, eMessage, e);
		if (e instanceof SkipException) {
			Reporter.log(SKIP_EXCEPTION_HTML_BEGIN + eMessage + SKIP_HTML_END1 + SKIP_HTML_END2);
			ExtentReporterUtil.skip(eMessage);
			ExtentReporterUtil.logStackTrace(e);
		} else {
			Reporter.log("<!--FAIL-->");
			Reporter.log(FAIL_HTML_BEGIN + eMessage + FAIL_HTML_END1 + FAIL_HTML_END2);
			ExtentReporterUtil.fail(eMessage);
			ExtentReporterUtil.logStackTrace(e);
		}
		throw e;
	}

	/**
	 * Asserts that a condition is true or false, depends upon the status. Then
	 * it will print the verified message if status is true, else stop the
	 * script and print the failed message
	 * 
	 * @param status
	 *            - boolean or expression returning boolean
	 * @param passmsg
	 *            -message to be logged when assert status is true
	 * @param failmsg
	 *            -message to be logged when assert status is false
	 */
	public static void assertThat(boolean status, String passmsg, String failmsg) {
		if (!status) {
			Log.fail(failmsg);
		} else {
			if(passmsg.contains("-")){
			}
			else{
				Log.message(passmsg);
			}

		}
	}

	/**
	 * Asserts that a condition is true or false, depends upon the status. Then
	 * it will print the verified message if status is true, else print the
	 * failed message in red color and continue the next step(not
	 * stopping/breaking the test script)
	 * 
	 * @param status
	 *            - boolean or expression returning boolean
	 * @param passmsg
	 *            -message to be logged when assert status is true
	 * @param failmsg
	 *            -message to be logged when assert status is false
	 */
	public static void softAssertThat(boolean status, String passmsg, String failmsg) {
		if (!status) {
			Log.failsoft(failmsg);
		} else {
			Log.message(passmsg);
		}
	}
}