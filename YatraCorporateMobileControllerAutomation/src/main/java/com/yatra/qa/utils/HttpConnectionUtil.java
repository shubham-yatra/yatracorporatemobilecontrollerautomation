package com.yatra.qa.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.*;
import org.apache.http.Consts;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.*;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONObject;

import com.yatra.qa.commonworkflows.ExecutionTimer;

public class HttpConnectionUtil {

	private HttpGet httpGet;
	private HttpPost httpPost;
	private HttpResponse response = null;
	private HttpClient client;
	private String line = "";
	private HttpDelete delete;
	private HttpPut httpPut;
	private StringBuffer result;
	public String ssoToken;

	public StringBuffer getResult(HttpResponse response) throws IOException {
		result = new StringBuffer();
		BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		while ((line = reader.readLine()) != null) {
			result.append(line);
		}
		return result;
	}

	public void getSetUp(String url) {
		Log.message("URL is: " + url);
		httpGet = new HttpGet(url);
		httpGet.addHeader("Cookie", "ssoToken=" + ssoToken);
		httpGet.addHeader("Content-Type", "application/json");
		client = HttpClientBuilder.create().build();
	}

	public void postSetUp(String url) {
		httpPost = new HttpPost(url);
	}

	public void postSetUp(String url, String request) throws UnsupportedEncodingException {
		httpPost = new HttpPost(url);
		Log.message("URL is: " + httpPost);
		httpPost.addHeader("Cookie", "ssoToken=" + ssoToken);
		httpPost.addHeader("Content-Type", "application/json");
		httpPost.setEntity(new StringEntity(request));
		client = HttpClientBuilder.create().build();
	}

	public void postSetUpForFormData(String url, List<NameValuePair> formdata) {
		httpPost = new HttpPost(url);
		UrlEncodedFormEntity entity = new UrlEncodedFormEntity(formdata, Consts.UTF_8);
		httpPost.setEntity(entity);
		httpPost.addHeader("Cookie", "ssoToken=" + ssoToken);
		httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded");
		client = HttpClientBuilder.create().build();
	}

	public void deleteSetUp(String url) {
		delete = new HttpDelete(url);
		delete.addHeader("Cookie", "ssoToken=" + ssoToken);
		delete.addHeader("Content-Type", "application/json");
		client = HttpClientBuilder.create().build();
	}

	public void putSetUp(String url, String request) throws UnsupportedEncodingException {
		httpPut = new HttpPut(url);
		httpPut.addHeader("Cookie", "ssoToken=" + ssoToken);
		httpPut.addHeader("Content-Type", "application/json");
		httpPut.setEntity(new StringEntity(request));
		client = HttpClientBuilder.create().build();
	}

	public JSONObject getFortrip(String url, String description)
			throws ClientProtocolException, IOException, InterruptedException {
		getSetUp(url);
		long startTime = ExecutionTimer.startTime();
		response = client.execute(httpGet);
		if (!description.contains("-"))
			Log.message(
					"time take by " + description + " API is " + ExecutionTimer.elapsedTime(startTime) + " seconds");
		validateApi(response, description);
		result = getResult(response);
		return new JSONObject(result.substring(result.indexOf("{")));
	}

	public JSONObject getFortrip(String request, String url, String description)
			throws ClientProtocolException, IOException, InterruptedException {
		getSetUp(url);
		long startTime = ExecutionTimer.startTime();
		response = client.execute(httpGet);
		Log.message("time take by " + description + " API is " + ExecutionTimer.elapsedTime(startTime) + " seconds");
		validateApi(response, description);
		result = getResult(response);
		return new JSONObject(result.substring(result.indexOf("{")));
	}

	public JSONObject getTripOnline(String url, String description)
			throws ClientProtocolException, IOException, InterruptedException {

		getSetUp(url);
		long startTime = ExecutionTimer.startTime();
		response = client.execute(httpGet);
		if (!description.contains("-"))
			Log.message(
					"time take by " + description + " API is " + ExecutionTimer.elapsedTime(startTime) + " seconds");
		validateApi(response, description);
		result = getResult(response);
		return new JSONObject(result.substring(result.indexOf("{")));
	}

	@SuppressWarnings("rawtypes")
	public JSONObject getOnlineHotel(String url, String description)
			throws ClientProtocolException, IOException, InterruptedException {
		getSetUp(url.toString());
		long startTime = ExecutionTimer.startTime();
		response = client.execute(httpGet);
		if (!description.contains("-"))
			Log.message(
					"time take by " + description + " API is " + ExecutionTimer.elapsedTime(startTime) + " seconds");
		validateApi(response, description);
		result = getResult(response);
		return new JSONObject(result.substring(result.indexOf("{")));
	}

	public JSONObject post(String request, String description)
			throws ClientProtocolException, IOException, InterruptedException {

		httpPost = new HttpPost(ReadProperties.getProperty("baseURI"));
		httpPost.setEntity(new StringEntity(request));
		client = HttpClientBuilder.create().build();
		long startTime = ExecutionTimer.startTime();
		response = client.execute(httpPost);
		if (!description.contains("-"))
			Log.message(
					"time take by " + description + " API is " + ExecutionTimer.elapsedTime(startTime) + " seconds");
		validateApi(response, description);
		result = getResult(response);
		return new JSONObject(result.substring(result.indexOf("{")));
	}

	public JSONObject postForTrip(String request, String url, String description)
			throws ClientProtocolException, IOException, InterruptedException {

		postSetUp(url, request);
		long startTime = ExecutionTimer.startTime();
		response = client.execute(httpPost);
		if (!description.contains("-"))
			Log.message(
					"time take by " + description + " API is " + ExecutionTimer.elapsedTime(startTime) + " seconds");
		validateApi(response, description);
		result = getResult(response);
		return new JSONObject(result.substring(result.indexOf("{")));
	}

	public JSONObject deleteProduct(String url, String request, String description)
			throws ClientProtocolException, IOException, InterruptedException {
		postSetUp(url, request);
		long startTime = ExecutionTimer.startTime();
		response = client.execute(httpPost);
		if (!description.contains("-"))
			Log.message(
					"time take by " + description + " API is " + ExecutionTimer.elapsedTime(startTime) + " seconds");
		validateApi(response, description);
		result = getResult(response);
		return new JSONObject(result.substring(result.indexOf("{")));
	}

	@SuppressWarnings({ "rawtypes" })
	public JSONObject postForTrip(String request, String url, HashMap<String, String> params, String description)
			throws ClientProtocolException, IOException, InterruptedException {

		String URI = ReadProperties.getProperty("TripUrl") + url;
		httpPost = new HttpPost(URI);
		Set entrySet = params.entrySet();
		Iterator itr = entrySet.iterator();

		while (itr.hasNext()) {
			Map.Entry p = (Map.Entry) itr.next();
			httpPost.addHeader(p.getKey().toString(), p.getValue().toString());
		}
		httpPost.addHeader("Cookie", "ssoToken=" + ssoToken);
		httpPost.addHeader("Content-Type", "application/json");
		httpPost.setEntity(new StringEntity(request));
		client = HttpClientBuilder.create().build();
		long startTime = ExecutionTimer.startTime();
		response = client.execute(httpPost);
		if (!description.contains("-"))
			Log.message(
					"time take by " + description + " API is " + ExecutionTimer.elapsedTime(startTime) + " seconds");
		validateApi(response, description);
		result = getResult(response);
		return new JSONObject(result.substring(result.indexOf("{")));
	}

	public JSONObject putForTrip(String request, String url, String description)
			throws ClientProtocolException, IOException, InterruptedException {
		putSetUp(url, request);
		long startTime = ExecutionTimer.startTime();
		response = client.execute(httpPut);
		if (!description.contains("-"))
			Log.message(
					"time take by " + description + " API is " + ExecutionTimer.elapsedTime(startTime) + " seconds");
		validateApi(response, description);
		result = getResult(response);
		return new JSONObject(result.substring(result.indexOf("{")));

	}

	public JSONObject put(String request, String url, LinkedHashMap<String, String> params, String description)
			throws ClientProtocolException, IOException, InterruptedException {
		putSetUp(url, request);
		Set entrySet = params.entrySet();
		Iterator itr = entrySet.iterator();

		while (itr.hasNext()) {
			Map.Entry p = (Map.Entry) itr.next();
			httpPut.addHeader(p.getKey().toString(), p.getValue().toString());
		}
		long startTime = ExecutionTimer.startTime();
		response = client.execute(httpPut);
		if (!description.contains("-"))
			Log.message(
					"time take by " + description + " API is " + ExecutionTimer.elapsedTime(startTime) + " seconds");
		validateApi(response, description);
		result = getResult(response);
		return new JSONObject(result.substring(result.indexOf("{")));

	}

	public JSONObject deleteForTrip(String request, String url, String description)
			throws ClientProtocolException, IOException {
		String URI = ReadProperties.getProperty("TripUrl") + url;

		delete = new HttpDelete(URI);
		delete.addHeader("Cookie", "ssoToken=" + ssoToken);
		delete.addHeader("Content-Type", "application/json");
		client = HttpClientBuilder.create().build();
		long startTime = ExecutionTimer.startTime();
		response = client.execute(delete);
		Log.message("time take by " + description + " API is " + ExecutionTimer.elapsedTime(startTime) + " seconds");
		validateApi(response, description);
		result = getResult(response);
		return new JSONObject(result.substring(result.indexOf("{")));

	}

	public void validateApi(HttpResponse response, String description) {
		Log.assertThat(response.getStatusLine().getStatusCode() == 200, description + " API successfully invoked",
				description + " API is down , status code = " + response.getStatusLine().getStatusCode() + " Status "
						+ response.getStatusLine().getReasonPhrase());
	}

	@SuppressWarnings("rawtypes")
	public HttpGet getURL(String url, LinkedHashMap<String, String> testdata)
			throws ClientProtocolException, IOException, InterruptedException {

		URIBuilder builder = new URIBuilder();
		builder.setPath(ReadProperties.getProperty("FlightUrlOnline") + url);
		Set entrySet = testdata.entrySet();
		Iterator it = entrySet.iterator();
		while (it.hasNext()) {
			Map.Entry param = (Map.Entry) it.next();
			builder.setParameter(param.getKey().toString(),
					param.getValue().toString().equals("null") ? "" : param.getValue().toString());
		}
		httpGet = new HttpGet(builder.toString());
		return httpGet;
	}

	public JSONObject postOnlineHotel(String url, List<NameValuePair> formdata, String description) throws IOException {

		httpPost = new HttpPost(ReadProperties.getProperty("HotelUrlOnline") + url);
		UrlEncodedFormEntity entity = new UrlEncodedFormEntity(formdata, Consts.UTF_8);
		httpPost.setEntity(entity);
		httpPost.addHeader("Cookie", "ssoToken=" + ssoToken);
		httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded");
		client = HttpClientBuilder.create().build();
		long startTime = ExecutionTimer.startTime();
		response = client.execute(httpPost);
		if (!description.contains("-"))
			Log.message(
					"time take by " + description + " API is " + ExecutionTimer.elapsedTime(startTime) + " seconds");
		validateApi(response, description);
		result = getResult(response);
		return new JSONObject(result.substring(result.indexOf("{")));

	}

	@SuppressWarnings("rawtypes")
	public String constructURL(String initials, String url, LinkedHashMap<String, String> params) {
		URIBuilder builder = new URIBuilder();
		builder.setPath(ReadProperties.getProperty(initials) + url);
		Set entrySet = params.entrySet();
		Iterator it = entrySet.iterator();
		while (it.hasNext()) {
			Map.Entry param = (Map.Entry) it.next();
			builder.setParameter(param.getKey().toString(),
					param.getValue() == null ? "null" : param.getValue().toString());
		}
		return builder.toString();

	}

	public JSONObject onlinePostForTrip(String request, String url, String description)
			throws ClientProtocolException, IOException, InterruptedException {

		postSetUp(url, request);
		long startTime = ExecutionTimer.startTime();
		response = client.execute(httpPost);
		Log.message("time take by " + description + " API is " + ExecutionTimer.elapsedTime(startTime) + " seconds");
		validateApi(response, description);
		result = getResult(response);
		return new JSONObject(result.toString());
	}

	@SuppressWarnings({ "rawtypes" })
	public JSONObject onlinePostForTrip(String request, String url, HashMap<String, String> params, String description)
			throws ClientProtocolException, IOException, InterruptedException {

		URIBuilder builder = new URIBuilder();
		String URL = description.contains("Online") ? ReadProperties.getProperty("FlightUrlOnline") + url
				: ReadProperties.getProperty("TripUrl") + url;
		builder.setPath(URL);
		Set entrySet = params.entrySet();
		Iterator it = entrySet.iterator();
		while (it.hasNext()) {
			Map.Entry param = (Map.Entry) it.next();
			builder.setParameter(param.getKey().toString(),
					param.getValue().toString().equals("null") ? "" : param.getValue().toString());
		}
		httpPost = new HttpPost(builder.toString());
		Log.message("URL is: " + httpPost);
		httpPost.addHeader("Cookie", "ssoToken=" + ssoToken);
		httpPost.addHeader("Content-Type", "application/json");
		httpPost.setEntity(new StringEntity(request));
		client = HttpClientBuilder.create().build();
		long startTime = ExecutionTimer.startTime();
		response = client.execute(httpPost);
		Log.message("time take by " + description + " API is " + ExecutionTimer.elapsedTime(startTime) + " seconds");
		validateApi(response, description);
		result = getResult(response);
		return new JSONObject(result.toString());
	}

	public JSONObject postOnlineFlight(String url, List<NameValuePair> formdata, String description)
			throws IOException {

		httpPost = new HttpPost(ReadProperties.getProperty("flightSubmitURL") + url);
		UrlEncodedFormEntity entity = new UrlEncodedFormEntity(formdata, Consts.UTF_8);
		httpPost.setEntity(entity);
		httpPost.addHeader("Cookie", "ssoToken=" + ssoToken);
		httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded");
		client = HttpClientBuilder.create().build();
		long startTime = ExecutionTimer.startTime();
		response = client.execute(httpPost);
		if (!description.contains("-"))
			Log.message(
					"time take by " + description + " API is " + ExecutionTimer.elapsedTime(startTime) + " seconds");
		validateApi(response, description);
		result = getResult(response);
		return new JSONObject(result.toString());

	}

	public JSONObject post(String request, String url, LinkedHashMap<String, String> params, String description)
			throws ClientProtocolException, IOException {
		postSetUp(url, request);
		Set entrySet = params.entrySet();
		Iterator itr = entrySet.iterator();

		while (itr.hasNext()) {
			Map.Entry p = (Map.Entry) itr.next();
			httpPost.addHeader(p.getKey().toString(), p.getValue().toString());
		}
		httpPost.addHeader("Cookie", "ssoToken=" + ssoToken);
		httpPost.addHeader("Content-Type", "application/json");
		long startTime = ExecutionTimer.startTime();
		response = client.execute(httpPost);
		if (!description.contains("-"))
			Log.message(
					"time take by " + description + " API is " + ExecutionTimer.elapsedTime(startTime) + " seconds");
		validateApi(response, description);
		result = getResult(response);
		return new JSONObject(result.toString());
	}

	public JSONObject getInternationalTripOnline(String url, String description)
			throws ClientProtocolException, IOException, InterruptedException {

		getSetUp(url);
		long startTime = ExecutionTimer.startTime();
		response = client.execute(httpGet);
		if (!description.contains("-"))
			Log.message(
					"time take by " + description + " API is " + ExecutionTimer.elapsedTime(startTime) + " seconds");
		validateApi(response, description);
		result = getResult(response);
		net.sf.json.JSONObject obj = new net.sf.json.JSONObject(result.toString());
		String str = obj.toString();
		return new JSONObject(str);

		// return new
		// JSONObjectIgnoreDuplicates(result.substring(result.indexOf("{")));
	}
}