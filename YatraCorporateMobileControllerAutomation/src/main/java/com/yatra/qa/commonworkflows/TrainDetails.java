package com.yatra.qa.commonworkflows;

import java.util.HashMap;

public class TrainDetails 
{
	public static HashMap<String, String> trainQuotaList = new HashMap<String,String>();
	public static HashMap<String, String> trainClassDetails = new HashMap<String,String>();
	public TrainDetails()
	{
		trainClassDetails.put("1A","First AC");
		trainClassDetails.put("2A","Second AC");
		trainClassDetails.put("3A","Third AC");
		trainClassDetails.put("FC","First Class");
		trainClassDetails.put("2S","Second Seating");
		trainClassDetails.put("CC","Chair Car");
		trainClassDetails.put("SL","Sleeper");
		
		trainQuotaList.put("GE", "General");
		trainQuotaList.put("TK", "Tatkal");
	}
	public static String getTrainQuotaListNames(String key)
	{
		new TrainDetails();
		return trainQuotaList.get(key);
	}
	public static String getTrainClassDetails(String key)
	{
		new TrainDetails();
		return trainClassDetails.get(key);
	}
}
