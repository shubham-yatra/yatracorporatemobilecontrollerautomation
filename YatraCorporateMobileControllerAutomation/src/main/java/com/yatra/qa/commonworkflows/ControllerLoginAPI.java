package com.yatra.qa.commonworkflows;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONObject;

import com.yatra.qa.utils.HttpConnectionUtil;
import com.yatra.qa.utils.Log;
import com.yatra.qa.utils.ReadProperties;

public class ControllerLoginAPI {
	private HashMap<String, String> testdata = new HashMap<String, String>();

	public ControllerLoginAPI(HashMap<String, String> testdata) {
		this.testdata = testdata;
	}

	LinkedHashMap<String, String> params = new LinkedHashMap<String, String>();
	HttpConnectionUtil con = new HttpConnectionUtil();
	private JSONObject validateLoginResponse;
	private String ssoToken = "";

	public void validateEmail() throws ClientProtocolException, IOException, InterruptedException {
		params.put("osVersion", ReadProperties.getProperty("osVersion"));
		params.put("deviceId", ReadProperties.getProperty("deviceId"));
		params.put("appId", ReadProperties.getProperty("appId"));
		params.put("sessionId", ReadProperties.getProperty("sessionId"));
		params.put("appVersion", ReadProperties.getProperty("appVersion"));
		params.put("emailID", testdata.get("email"));
		String endpoint = "/" + testdata.get("tenant").trim() + "/v2/checkValidEmail.htm";
		String url = con.constructURL("ControllerURL", endpoint, params);
		JSONObject validateEmailResponse = con.postForTrip("", url, "ValidateEmail");
		Log.message("ValidateEmail response : " + validateEmailResponse);

		Log.assertThat(validateEmailResponse.getInt("resCode") == Integer.parseInt(testdata.get("status")),
				"This API is returning correct Response Code", "This API is returning incorrect Response Code");
		Log.message("Interaction ID: " + validateEmailResponse.getString("interactionId"));
		Log.assertThat(!validateEmailResponse.getString("interactionId").isEmpty(),
				"The API is successfully returned InteractionID as " + validateEmailResponse.getString("interactionId"),
				"The API is returning Inteaction ID as null");

	}

	// validation for Login flow

	public void validateLogin() throws Exception {

		params.put("osVersion", ReadProperties.getProperty("osVersion"));
		params.put("deviceId", ReadProperties.getProperty("deviceId"));
		params.put("appId", ReadProperties.getProperty("appId"));
		params.put("sessionId", ReadProperties.getProperty("sessionId"));
		params.put("appVersion", ReadProperties.getProperty("appVersion"));
		params.put("email", testdata.get("email").equals("null") ? null : testdata.get("email"));
		params.put("authProvider", "YATRA");
		params.put("password", testdata.get("password").equals("null") ? null : testdata.get("password"));

		int resCode = 0;
		boolean isTrip = true;
		String approval_method = "";
		Log.message("Email ID is: " + testdata.get("email"));
		Log.message("Approval method: " + approval_method);
		approval_method = DatabaseWorkFlows.getAgencyConfigFlow(testdata.get("email"));

		Log.message("Approval method: " + approval_method);
		if (!testdata.get("email").isEmpty() && approval_method != null
				&& approval_method.equalsIgnoreCase("ITINERARY")) {
			resCode = 768;
			isTrip = false;
			Log.message("This Corporate is on non Trip flow");
		}

		String endpoint = "/" + testdata.get("tenant").trim() + "/v3/corporateLogin.htm";
		String url = con.constructURL("ControllerURL", endpoint, params);
		validateLoginResponse = con.postForTrip("", url, "ValidateLogin");
		Log.message("The API Request is : " + url);
		Log.message("ValidateLogin response : " + validateLoginResponse);
		if (isTrip)
			resCode = Integer.parseInt(testdata.get("status"));

		if (validateLoginResponse.getInt("resCode") == 200) {
			ssoToken = validateLoginResponse.getJSONObject("response").getString("ssoToken");
			validateLogin(ssoToken);

		}
		Log.assertThat(resCode == validateLoginResponse.getInt("resCode"),
				"This API is returning correct Response Code", "This API is returning incorrect Response Code"
						+ " expected =" + resCode + " actual=" + validateLoginResponse.getInt("resCode"));
		Log.message("Interaction ID: " + validateLoginResponse.getString("interactionId"));
		Log.assertThat(!validateLoginResponse.getString("interactionId").isEmpty(),
				"The API is successfully returned InteractionID as " + validateLoginResponse.getString("interactionId"),
				"The API is returning Inteaction ID as null");

	}

	public void validateLogout() throws ClientProtocolException, IOException, InterruptedException {

		params.put("osVersion", ReadProperties.getProperty("osVersion"));
		params.put("deviceId", ReadProperties.getProperty("deviceId"));
		params.put("appId", ReadProperties.getProperty("appId"));
		params.put("sessionId", ReadProperties.getProperty("sessionId"));
		params.put("appVersion", ReadProperties.getProperty("appVersion"));
		ssoToken = validateLoginResponse.getJSONObject("response").getString("ssoToken");

		switch (testdata.get("Description")) {

		case "LogoutWithWrongSsoToken":
			ssoToken = ssoToken + "dummyText";
			break;
		case "LogoutWithNullSsoToken":
			ssoToken = "";
			break;
		case "LogoutWithBlankSsoToken":
			ssoToken = "";
			break;
		}

		// params.put("ssoToken",testdata.get("appId"));
		params.put("ssoToken", ssoToken);

		String endpoint = "/" + testdata.get("tenant").trim() + "/logout.htm";
		String url = con.constructURL("ControllerURL", endpoint, params);
		if (testdata.get("Description").equals("LogoutWithNullSsoToken"))
			url += null;

		JSONObject validateLogoutResponse = con.postForTrip("", url, "ValidateLogout");
		Log.message("Request URL: " + url);
		Log.message("Logout Response : " + validateLogoutResponse);

		Log.assertThat(validateLogoutResponse.getInt("resCode") == Integer.parseInt(testdata.get("logoutstatus")),
				"This API is returning correct Response Code", "This API is returning incorrect Response Code");
		Log.message("Interaction ID: " + validateLogoutResponse.getString("interactionId"));
		Log.assertThat(!validateLogoutResponse.getString("interactionId").isEmpty(),
				"The API is successfully returned InteractionID as "
						+ validateLogoutResponse.getString("interactionId"),
				"The API is returning Inteaction ID as null");

	}

	public void validateForgotPassword() throws ClientProtocolException, IOException, InterruptedException {

		params.put("osVersion", ReadProperties.getProperty("osVersion"));
		params.put("deviceId", ReadProperties.getProperty("deviceId"));
		params.put("appId", ReadProperties.getProperty("appId"));
		params.put("sessionId", ReadProperties.getProperty("sessionId"));
		params.put("appVersion", ReadProperties.getProperty("appVersion"));
		params.put("emailId", testdata.get("email"));
		String endpoint = "/" + testdata.get("tenant").trim() + "/corporateForgotPassword.htm";
		String url = con.constructURL("ControllerURL", endpoint, params);
		JSONObject validateEmailResponse = con.postForTrip("", url, "ValidateEmail");
		Log.message("Request URL: " + url);
		Log.message("ValidateEmail response : " + validateEmailResponse);

		Log.assertThat(validateEmailResponse.getInt("resCode") == Integer.parseInt(testdata.get("status")),
				"This API is returning correct Response Code", "This API is returning incorrect Response Code");
		Log.message("Interaction ID: " + validateEmailResponse.getString("interactionId"));
		Log.assertThat(!validateEmailResponse.getString("interactionId").isEmpty(),
				"The API is successfully returned InteractionID as " + validateEmailResponse.getString("interactionId"),
				"The API is returning Inteaction ID as null");

	}
	/*
	 * http://172.16.6.227:6060/ccwebapp/mobile/common/ccommonandroid/v3/
	 * corporateLogin.htm
	 * 
	 * &tripId= &ssoToken=2e3af967-4abd-4748-a27a-3bf86286ce6a
	 * 
	 */

	public void validateLogin(String ssoToken) throws ClientProtocolException, IOException, InterruptedException {

		params.put("osVersion", ReadProperties.getProperty("osVersion"));
		params.put("deviceId", ReadProperties.getProperty("deviceId"));
		params.put("appId", ReadProperties.getProperty("appId"));
		params.put("sessionId", ReadProperties.getProperty("sessionId"));
		params.put("appVersion", ReadProperties.getProperty("appVersion"));
		params.put("email", testdata.get("email").equals("null") ? null : testdata.get("email"));
		params.put("ssoToken", ssoToken);
		int resCode = 0;

		String endpoint = "/" + testdata.get("tenant").trim() + "/v3/corporateLogin.htm";
		String url = con.constructURL("ControllerURL", endpoint, params);
		validateLoginResponse = con.postForTrip("", url, "ValidateLogin");
		Log.message("The API Request is : " + url);
		Log.message("ValidateLogin response : " + validateLoginResponse);
		resCode = Integer.parseInt(testdata.get("status"));

		Log.assertThat(resCode == validateLoginResponse.getInt("resCode"),
				"This Login API with SSO Token is returning correct Response Code",
				"This Login API with SSO Token is returning incorrect Response Code");
		Log.message("Interaction ID: " + validateLoginResponse.getString("interactionId"));
		Log.assertThat(!validateLoginResponse.getString("interactionId").isEmpty(),
				"The Login API with SSO Token is successfully returned InteractionID as "
						+ validateLoginResponse.getString("interactionId"),
				"The API is returning Inteaction ID as null");

	}

}
