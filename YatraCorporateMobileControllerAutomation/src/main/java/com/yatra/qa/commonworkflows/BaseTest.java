package com.yatra.qa.commonworkflows;
import com.yatra.qa.datahelpers.TestDataGenerator;
import com.yatra.qa.utils.HttpConnectionUtil;
import com.yatra.sso.login.service.beans.UserLoginResponseWO;
import com.yatra.sso.login.service.beans.UserLogoutResponseWO;

import org.json.JSONObject;
import org.testng.ITest;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import org.testng.annotations.*;

public class BaseTest implements ITest{
	private String testInstanceName = "";
	public HashMap<String , String> testdata = new HashMap<String , String>();
	/*public static UserLoginResponseWO loginResponse;*/
	public static JSONObject loginResponse;
	//public UserLogoutResponseWO logoutResponseWO;
	public JSONObject logoutResponse;
	public static HttpConnectionUtil con;

	@BeforeSuite(alwaysRun = true)
	public static void beforeTest() {
		
		con = new HttpConnectionUtil();
	}
	
	@AfterTest
	public void afterTest() {
	}

	@Override
	public String getTestName() {
		return testInstanceName;
	}

	private void setTestName(String anInstanceName) {
		testInstanceName = anInstanceName;
	} 
	public void extractTestNameFromParameters(HashMap<String, String> testdata) throws InterruptedException{
		//setTestName(method+"_"+RandomUtil.generateRandom());
		Throwable t=new Throwable();
		System.out.println();
		String prefix=t.getStackTrace()[1].getMethodName();
		String suffix=testdata.get("Description").replace(" ", "_");
		setTestName(prefix+"_"+suffix);
	}
	@SuppressWarnings({ "unused", "rawtypes" })
	@BeforeMethod(alwaysRun = true)
	public void nameChanger(Method method, Object[] parameters){

		UseAsTestName useAsTestName = method.getAnnotation(UseAsTestName.class);
		List data = (List) parameters[0];
		testdata=TestDataGenerator.getHashMap(data);	
		setTestName(testdata.get("Description"));
	}
}
