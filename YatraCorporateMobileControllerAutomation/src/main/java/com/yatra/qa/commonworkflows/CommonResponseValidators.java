package com.yatra.qa.commonworkflows;

import org.json.JSONObject;
import com.yatra.qa.utils.Log;

public class CommonResponseValidators {

	private static boolean validated=false;
	
	public static void validate(JSONObject response){
		validated = validateIsNotNull(response);
		
		Log.assertThat(validated, "The Response is not null", "The Response is null");
		
		validated =validateResponseCode(response);
		Log.assertThat(validated,
				"The Response code is 200 , hence validated", 
				"The Response code is not 200 Hence not validated");	
	}
	
	public static boolean validateIsNotNull(JSONObject response){
		return response != null ? true : false;
	}
	public static boolean validateResponseCode(JSONObject response){
		if(response.has("responseStatus")){
			JSONObject status = (JSONObject)response.get("responseStatus");
			return status.get("code").toString().equals("200") ? true: false;
		}
		else{
			return response.has("resCode") ? true : false;
		}
	}
}
