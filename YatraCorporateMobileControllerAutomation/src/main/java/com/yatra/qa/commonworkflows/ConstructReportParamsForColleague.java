
package com.yatra.qa.commonworkflows;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class ConstructReportParamsForColleague {
	public List<ProfileRM> profileRM = new ArrayList<ProfileRM>();
	public List<BookingRM> bookingRM = new ArrayList<BookingRM>();
	
	public ConstructReportParamsForColleague(JSONObject profilerm, JSONArray bookingrms)
	{
		ProfileRM profile ;
		BookingRM booking ;
		JSONArray profileRMArray = (JSONArray) profilerm.get("profileRM");
		for(int i=0 ;i <profileRMArray.length() ; i++){
			JSONObject obj =(JSONObject) profileRMArray.get(i);
			profile = new ProfileRM("" , obj.getString("code"));
			profileRM.add(profile);
		}
		for(int i=0 ;i <bookingrms.length() ; i++){
			JSONObject obj =(JSONObject) bookingrms.get(i);
			booking= new BookingRM("" , obj.getString("code"));
			bookingRM.add(booking);
		}
	}
}
class ProfileRM
{
	public String value;
	public String code;
	
	public ProfileRM(String value , String code){
		this.value = value;
		this.code = code;
	}	
}
class BookingRM
{
	public String value;
	public String code;
	
	public BookingRM(String value , String code){
		this.value = value;
		this.code = code;
	}	
}