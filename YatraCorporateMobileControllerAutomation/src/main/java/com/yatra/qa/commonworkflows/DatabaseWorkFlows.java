package com.yatra.qa.commonworkflows;

import com.yatra.qa.utils.DBUtil;

public class DatabaseWorkFlows {

	static DBUtil dbutil = new DBUtil();
	static String queryForParentId = "select parent_id from yatra.b2b_corporate where email =";
	static String queryForEmail = "select email from yatra.b2b_corporate where id=";

	public static String getAdminUserEmail(String email) throws Exception {
		dbutil.connect();
		int parentId = 0;
		parentId = dbutil.executee(queryForParentId + "'" + email + "'", "parent_id");
		String adminEmail = "";

		adminEmail = dbutil.execute(queryForEmail + "'" + parentId + "'", "email");
		return adminEmail;

	}

	public static boolean isApprovalRequired(String email) throws Exception {
		String adminEmail = getAdminUserEmail(email);
		String query = "select * from b2b_features_config_details"
				+ " where code = 'approval_enabled' and agency_email='" + adminEmail + "'";
		String value = dbutil.execute(query, "value");
		if (value.equals("1")) {
			return true;
		} else {
			return false;
		}
	}

	public static String getAgencyConfigFlow(String email) throws Exception {
		String adminEmail = getAdminUserEmail(email);
		String query = "select approval_method from b2b_corporate_company_configuration where agency_email='"
				+ adminEmail + "'";
		String value = dbutil.execute(query, "approval_method");
		return value;
	}

}
