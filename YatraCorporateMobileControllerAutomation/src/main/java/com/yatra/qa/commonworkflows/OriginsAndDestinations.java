
package com.yatra.qa.commonworkflows;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class OriginsAndDestinations {

	public static List<String> countrySource = new ArrayList<String>();
	public static List<String> countryDest = new ArrayList<String>();
	public static List<String> nationalOrigin = new ArrayList<String>();
	public static List<String> nationalDest = new ArrayList<String>();
	public static List<String> domesticOrigin = new ArrayList<String>();
	public static List<String> domesticDest = new ArrayList<String>();
	public static List<String> hotel = new ArrayList<String>();
	public static HashMap<String,String> data = new HashMap<String,String>(); 
	
	public OriginsAndDestinations()
	{
		countrySource.add("New Delhi_DEL_IN_India_Indira Gandhi");
		countrySource.add("New York_NYC_US_United States_LaGuardia");
		
		countryDest.add("London_LON_UK_United Kingdom_London City");
		countryDest.add("Singapore_SIN_SIN_Singapore_Singapore Changi Airport");
		
		nationalOrigin.add("banglore_BLR_IN_India_Kempegowda International Airport");
		nationalOrigin.add("Mumbai_BOM_IN_India_Chhatrapati Shivaji International Airport");
		
		nationalDest.add("Chennai_MAA_IN_India_Chennai airport");
		nationalDest.add("New Delhi_DEL_IN_India_Indira Gandhi");
		
		domesticOrigin.add("DELHI");
		domesticOrigin.add("LUCKNOW");
		domesticOrigin.add("MUMBAI");
		domesticOrigin.add("BANGLORE");
		domesticOrigin.add("CHENNAI");
		domesticOrigin.add("BHOPAL");
		
		domesticDest.add("MANGALORE");
		domesticDest.add("HYDERABAD");
		domesticDest.add("MYSORE");
		domesticDest.add("PUNE");
		domesticDest.add("KANPUR");
		domesticDest.add("ALLAHABAD");
		
		hotel.add("New York_New York (and Vicinity), New York_United States of America_US");
		hotel.add("Los Angeles_Los Angeles_United States of America_US");
		hotel.add("Berlin_Berlin_Germany_GE");
		hotel.add("Paris_Paris_France_FR");
		

		data.put("DEL","New Delhi_DEL_IN_India_Indira Gandhi");
		data.put("MAA","Chennai_MAA_IN_India_Chennai airport");
		data.put("BOM","Mumbai_BOM_IN_India_Chhatrapati Shivaji International Airport");  
		data.put("NYC","New York_NYC_US_United States_LaGuardia");
		data.put("LON","London_LON_UK_United Kingdom_London City");
	}
	
	
	public static String getCountrySourceDetails(){
		new OriginsAndDestinations();
		int index = RandomUtil.generateRandomIndex(countrySource.size());
		return countrySource.get(index);
	}
	public static String getCountryDestDetails(){
		new OriginsAndDestinations();
		int index = RandomUtil.generateRandomIndex(countryDest.size());
		return countryDest.get(index);
	}
	
	public static String getVisaDetails(){
		return "";
	}
	public static String getDomesticOriginCity()
	{
		new OriginsAndDestinations();
		int index = RandomUtil.generateRandomIndex(domesticOrigin.size());
		return domesticOrigin.get(index);
	}
	public static String getDomesticDestCity()
	{
		new OriginsAndDestinations();
		int index = RandomUtil.generateRandomIndex(domesticDest.size());
		return domesticDest.get(index);
	}
	public static String getNationalOriginCityDetails()
	{
		new OriginsAndDestinations();
		int index = RandomUtil.generateRandomIndex(nationalOrigin.size());
		return nationalOrigin.get(index);
	}
	public static String getNationalDestCityDetails()
	{
		new OriginsAndDestinations();
		int index = RandomUtil.generateRandomIndex(nationalDest.size());
		return nationalDest.get(index);
	}
	public static String getHotelDetails(){
		new OriginsAndDestinations();
		int index = RandomUtil.generateRandomIndex(hotel.size());
		return hotel.get(index);
	}
public static String getOriginCityCodeDetails(String originCode){
		
		new OriginsAndDestinations();
		String originValues = null;
		if(data.containsKey(originCode))
		{
			originValues = OriginsAndDestinations.data.get(originCode);
		}
		return originValues;	
	}
public static String getDestCityCodeDetails(String originCode){
	
	new OriginsAndDestinations();
	String DestinationValues = null;
	if(data.containsKey(originCode))
	{
		DestinationValues = OriginsAndDestinations.data.get(originCode);
	}
	return DestinationValues;	
}
}

