package com.yatra.qa.dataproviders;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;

import com.yatra.qa.datahelpers.TestDataGenerator;

public class DataProviderUtil  {
			

	
	@DataProvider(name = "TestData")
	public static Object[][] getData(ITestContext context) throws Exception {
		String sheetname,dataDriver,workBook,tenant;
		Object[][] OW_SRP = null;
		sheetname = context.getCurrentXmlTest().getParameter("sheet");
		dataDriver = context.getCurrentXmlTest().getParameter("DataDriver");
		workBook = context.getCurrentXmlTest().getParameter("workBookName");
		TestDataGenerator testdata = new TestDataGenerator(workBook , sheetname ,dataDriver);
		try{
			OW_SRP = testdata.getTestDataForSanityTest();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return OW_SRP;
	}

@DataProvider(name ="")
public Object[][] getBookingStatusList()
{
	return new Object[][]{
		{
			new String("Completed")
			},
		{
			new String("Upcoming")
			}
	};
}
}
