package com.yatra.qa.testscripts;

import java.util.HashMap;
import java.util.List;

import org.testng.annotations.Test;

import com.yatra.qa.commonworkflows.BaseTest;
import com.yatra.qa.commonworkflows.ControllerLoginAPI;
import com.yatra.qa.datahelpers.TestDataGenerator;
import com.yatra.qa.dataproviders.DataProviderUtil;
import com.yatra.qa.utils.Log;

public class ValidateLoginTest extends BaseTest {

	@Test(dataProvider = "TestData", dataProviderClass = DataProviderUtil.class, groups = { "LoginTest" })
	public void checkResponseStatus(List data) throws Exception {

		HashMap<String, String> testdata = TestDataGenerator.getHashMap(data);

		Log.testCaseInfo(testdata);

		ControllerLoginAPI controllerApiObject = new ControllerLoginAPI(testdata);
		try {
			controllerApiObject.validateLogin();

		} catch (Exception e) {

			Log.exception(e);
		}

		finally {
			Log.endTestCase();

		}
	}

}
