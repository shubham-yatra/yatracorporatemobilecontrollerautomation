package com.yatra.qa.testscripts;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import org.apache.http.client.ClientProtocolException;
import org.testng.ITestContext;
import org.testng.annotations.Test;

import com.yatra.qa.commonworkflows.BaseTest;
import com.yatra.qa.commonworkflows.ControllerLoginAPI;
import com.yatra.qa.datahelpers.TestDataGenerator;
import com.yatra.qa.dataproviders.DataProviderUtil;
import com.yatra.qa.utils.Log;

public class ValidateLogoutTest  extends BaseTest{
	
	

	@Test(dataProvider = "TestData" , dataProviderClass = DataProviderUtil.class ,groups={"ValidateLogoutTest"})
	public void checkResponseStatus(List data) throws Exception {

		HashMap<String, String> testdata = TestDataGenerator.getHashMap(data);	
		Log.testCaseInfo(testdata);
		ControllerLoginAPI controllerApiObject= new ControllerLoginAPI(testdata);
		controllerApiObject.validateLogin();
		
		controllerApiObject.validateLogout();;
		Log.endTestCase();

	}
}
